const express = require('express');

const app = express();

app.set('view engine','jade');

app.set('views', './views');

app.get('/', (req, res) => { 
    res.sendFile(__dirname + '/index.html');
}); 

app.get('/api/courses',(req,res)=>{
    res.send([1,2,3]);
});

app.get('/alunos', (req, res) => {

    const {Aluno} = require('./alunos');

    const aluno = [
        {numero: 1, name: "Tiago", genero: "m"}
    ];
    
    res.send(aluno);

    res.status(200);
});

app.get('/jade', (req, res) => {

    res.render('index', {
        title: 'PP',
        messageTitle: 'Jade',
        messageText: 'Exemplo inicio'
    })

});

app.listen(8000, () => console.log('Example app listening on port 8000!'));
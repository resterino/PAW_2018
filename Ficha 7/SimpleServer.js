// Load HTTP module
var http = require("http");
var fs= require('fs');

fs.readFile('./index.html',(err,file)=>{
    if(err){
        throw err;
    }
    var Server=http.createServer((request,response)=>{
        response.writeHead(200, {'Content-Type': 'text/html'});
        fs.readFile('./index.html');
        response.end(file);
    }).listen(8000);
});


console.log('Server running at http://127.0.0.1:8000/');